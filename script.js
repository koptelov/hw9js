/**1
 * Опишіть, як можна створити новий HTML тег на сторінці.
 
  let div = document.createElement("div");
 div.className = "new";
 div.innerHTML = "Hello World ";
 *document.body.append(div);
 
 *2.  
 *Опишіть, що означає перший параметр функції
  insertAdjacentHTML і опишіть можливі варіанти
   цього параметра.

Перший параметр відповідає куди саме буде вставленний другий параметр
'beforebegin': до самого element(до відкриваючого тега).
'afterbegin': одразу після відкривающего тега element(перед першим потомком).
'beforeend': одразу перед закривающіи тегом element(після останього потомка).
'afterend': після element(після закривающего тега).
 
3
Як можна видалити елемент зі сторінки?

node.remove() – видаляє node.

 */

// масив
let country = ["Ukraine", "Germania", "England", "USA", "Canada", "France"];

//  ФункціЯ, яка прийматиме на вхід масив
//  і опціональний другий аргумент parent - DOM-елемент, до якого
//  буде прикріплений список (по дефолту має бути document.body.
function createList(arr, parent) {
  let ul = document.createElement("ul");
  for (let i = 0; i < arr.length; i++) {
    let li = document.createElement("li");
    li.innerHTML = arr[i];
    ul.appendChild(li);
  }
  if (parent) {
    parent.appendChild(ul);
  } else {
    document.body.appendChild(ul);
  }
}
// запуск функції
createList(country, document.body);

// .........................................................

// масив
let city = ["Kyiv", "Berlin", "London", "Washington", "Ottawa", "Paris"];

// ФункціЯ, яка прийматиме на вхід масив
//  і опціональний другий аргумент parent - DOM-елемент, до якого
//  буде прикріплений список (по дефолту має бути document.body.
function createList(arr, parent) {
  let ul = document.createElement("ul");
  for (let i = 0; i < arr.length; i++) {
    let li = document.createElement("li");
    li.innerHTML = arr[i];
    ul.appendChild(li);
  }
  if (parent) {
    parent.appendChild(ul);
  } else {
    document.body.appendChild(ul);
  }
}

// запуск функції
createList(city, document.body);
